local awful = require("awful")
local gears = require("gears")
local tonumber = tonumber

local daemon

local audio = { sink = {}, source = {} }

function audio.sink:SetVolume(value)
    awful.spawn.with_shell("wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ " .. value .. "%")
end

function audio.sink:RaiseVolume(step)
    awful.spawn.with_shell("wpctl set-mute @DEFAULT_AUDIO_SINK@ 0; wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ " .. (step or 2) .. "%+")
end

function audio.sink:LowerVolume(step)
    awful.spawn.with_shell("wpctl set-mute @DEFAULT_AUDIO_SINK@ 0; wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ " .. (step or 2) .. "%-")
end

function audio.sink:ToggleMute()
    awful.spawn.with_shell("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle")
end

function audio.source:SetVolume(value)
    awful.spawn.with_shell("wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ " .. value .. "%")
end

function audio.source:RaiseVolume(step)
    awful.spawn.with_shell("wpctl set-mute @DEFAULT_AUDIO_SINK@ 0; wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ " .. (step or 2) .. "%+")
end

function audio.source:LowerVolume(step)
    awful.spawn.with_shell("wpctl set-mute @DEFAULT_AUDIO_SINK@ 0; wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ " .. (step or 2) .. "%-")
end

function audio.source:ToggleMute()
    awful.spawn.with_shell("wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle")
end

local function on_device_change(self, device_type)
    if device_type == "sink" then
        awful.spawn.easy_async_with_shell("wpctl get-volume @DEFAULT_AUDIO_SINK@", function(stdout)
            local volume = tonumber(stdout:match("%d+.%d+")) * 100
            local muted = stdout:match("MUTED")

            self:emit_signal("sink::metadata", gears.math.round(volume), muted)
        end)
    elseif device_type == "source" then
        awful.spawn.easy_async_with_shell("wpctl get-volume @DEFAULT_AUDIO_SOURCE@", function(stdout)
            local volume = tonumber(stdout:match("%d+.%d+")) * 100
            local muted = stdout:match("MUTED")

            self:emit_signal("source::metadata", gears.math.round(volume), muted)
        end)
    end        
end

local function new()
    local ret = gears.object()

    gears.table.crush(ret, audio, true)
    
    awful.spawn.easy_async_with_shell("pkill -f pactl subscribe", function()
        awful.spawn.with_line_callback("pactl subscribe", {
            stdout = function(line)
                if line:match("Event 'change' on sink #") then
                    on_device_change(ret, "sink")
                elseif line:match("Event 'change' on source #") then
                    on_device_change(ret, "source")
                end
            end
        })
    end)

    gears.timer.delayed_call(function()
        on_device_change(ret, "sink")
        on_device_change(ret, "source")
    end)
    
    return ret
end

daemon = new()

return daemon
