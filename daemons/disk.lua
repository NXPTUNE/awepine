local awful = require("awful")
local gears = require("gears")

local daemon

local function new()
    local ret = gears.object()

    gears.timer {
        callback = function()
            awful.spawn.easy_async_with_shell("df -B 1MB / /home | tail -n 2", function(stdout)
                local root = {}
                local home = {}

                root.dev, root.total, root.usage, root.available, root.percentage, root.mount = stdout:match("(/dev/%w+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%%%s+(/)")
                home.dev, home.total, home.usage, home.available, home.percentage = stdout:match("(/dev/%w+)%s+(%d+)%s+(%d+)%s+(%d+)%s+(%d+)%%%s+(/home)")

                ret:emit_signal("request::metadata", root, home)
            end)
        end,
        timeout = 60,
        autostart = true,
        call_now = true
    }

    return ret
end

daemon = new()

return daemon
