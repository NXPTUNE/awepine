local awful = require("awful")
local gears = require("gears")

local daemon

local function new()
    local ret = gears.object()

    gears.timer {
        callback = function()
            awful.spawn.easy_async_with_shell("free -m | tail -n 2", function(stdout)
                local memory = {}
                local swap = {}
                
                memory.total, memory.usage, memory.available = stdout:match("Mem:%s+(%d+)%s+(%d+)%s+%d+%s+%d+%s+%d+%s+(%d+)")
                swap.total, swap.usage, swap.free = stdout:match("Swap:%s+(%d+)%s+(%d+)%s+(%d+)")

                memory.percentage = gears.math.round(memory.usage / memory.total * 100)

                ret:emit_signal("request::metadata", memory, swap)
            end)
        end,
        timeout = 10,
        autostart = true,
        call_now = true
    }
    
    return ret
end

daemon = new()

return daemon
