local awful = require("awful")
local gears = require("gears")
local autostart = require("config.settings").autostart
local capi = { awesome = awesome }
local pairs = pairs

local daemon

local system = {}

function system:Shutdown()
    capi.awesome.spawn("systemctl poweroff", false)
end

function system:Restart()
    capi.awesome.spawn("systemctl reboot", false)
end

function system:Suspend()
    capi.awesome.spawn("systemctl suspend", false)
end

function system:Exit()
    capi.awesome.quit()
end

function system:Refresh()
    capi.awesome.restart()
end

local function new()
    local ret = gears.object()
        
    gears.table.crush(ret, system, true)
    
    gears.timer.delayed_call(function()
        for _, cmd in pairs(autostart) do awful.spawn.with_shell(cmd) end    
    end)
    
    gears.timer {
        callback = function()        
            awful.spawn.easy_async_with_shell("uptime -p; echo -ne 'processes: '; ps -e | wc -l", function(stdout)
                local uptime = {}
                
                uptime.day, uptime.hour, uptime.min = stdout:match("up%s+(%d+)%sdays%s(%d+)%shours%s(%d+)minutes")

                local ps = stdout:match("processes:%s+(%d+)")
                
                ret:emit_signal("request::metadata", uptime, ps)
            end)
        end,
        timeout = 60,
        autostart = true,
        call_now = true
    }

    return ret
end

daemon = new()

return daemon
