local awful = require("awful")
local gears = require("gears")
local settings = require("config.settings")
local string = string

local daemon

local weather = {}

function weather:get()
    awful.spawn.easy_async_with_shell("curl 'wttr.in/" .. settings.city .. "?M&format=%l:+_%C_+%t+%f+%h+%w+%u,+%T'", function(stdout)
        local location, condition, temp, feels_like, humidity, wind, uv, time = stdout:match("(%w+):%s+_(.*)_%s+([+-]%d+°C)%s+([+-]%d+°C)%s+(%d+)%%.*(%d+)m/s%s+(%d+),%s+(%d+:%d+)")

        self:emit_signal("request::metadata", location, condition:gsub("%f[%a].", string.upper), temp, feels_like, humidity, wind, uv, time)
    end)
end

local function new()
    local ret = gears.object()

    gears.table.crush(ret, weather, true)
    
    gears.timer {
        callback = function() ret:get() end,
        timeout = 1800,
        autostart = true,
        call_now = true
    }

    return ret
end

daemon = new()

return daemon
